<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon_1.ico">

        <title>Ubold - Responsive Admin Dashboard Template</title>
        
        <!--calendar css-->
        <link href="<?php echo base_url('assets/plugins/fullcalendar/css/fullcalendar.min.css'); ?>" rel="stylesheet" />

        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/responsive.css');?>" rel="stylesheet" type="text/css" />


        <!-- DataTables -->
    <link href="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet'); ?>" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet'); ?>" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/responsive.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/scroller.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/dataTables.colVis.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('assets/plugins/datatables/fixedColumns.dataTables.min.css'); ?>" rel="stylesheet" type="text/css"/>

        <!-- iziToast -->
        <link href="<?php echo base_url('assets/iziToast-master/dist/css/iziToast.min.css');?>" rel="stylesheet" type="text/css" />
          <!-- iziToast -->
        <link href="<?php echo base_url('assets/custom/admin/css/style.css');?>" rel="stylesheet" type="text/css" />
        
        <script src="<?php echo base_url('assets/iziToast-master/dist/js/iziToast.min.js');?>"></script>

        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    
            <!-- /Right-bar -->


       