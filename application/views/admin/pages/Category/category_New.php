  <div class="modal-header"> 
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h4 class="modal-title"> New Category </h4> 
                </div> 
 <div class="modal-body">  
  <div class="row"> 
        <div class="col-md-12"> 
            <form class="form-horizontal" id="brandReg" role="form">                                    
	             <div class="form-group">
	                            <label class="col-md-4 control-label"> Category Name </label>
	                                <div class="col-md-6">
	                                    <input name="categoryName" type="text" class="form-control" placeholder="eg. Mr.Smith">
	                                </div>
	              </div>

	                         <div class="form-group">
	                            <label class="col-md-4 control-label">  </label>
	                                <div class="col-md-6">
	                                    <button type="button" class="btn btn-primary fa fa-save" onclick="setData('<?php echo base_url('admin/category/add'); ?>','brandReg')" > Save </button>
	                                    <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                                </div>
	                        </div>
	                                            	                                         	                           
	                       </form>
		             </div>      
		        </div>  
		    </div>