<div class="row">
		
	<div class="panel panel-border panel-primary">
		
		<div class="panel-heading">
			<h3 class="panel-title"> Staff List </h3>
		</div>
		<div class="panel-body">

			<div class="form-group">
				<button class="btn btn-primary waves-effect waves-light fa fa-plus" data-toggle="modal" data-target="#con-close-modal" type="button"> Category </button>
				<button class="btn btn-primary waves-effect waves-light fa fa-plus" data-toggle="modal" data-target="#con-close-modal" type="button"> Brand </button>
			</div>


			 <form class="form-horizontal" id="staffRegistraion" role="form">                                    
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Name </label>
	                                <div class="col-md-7">
	                                    <input name="name" type="text" class="form-control" placeholder="eg. Mr.Smith">
	                                </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Username </label>
	                                <div class="col-md-7">
	                                    <input name="username" type="text" class="form-control" placeholder="smith23">
	                                </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Password </label>
	                                <div class="col-md-7">
	                                    <select>
	                                    	<option> MK </option>
	                                    	<option> CNC </option>
	                                    </select>
	                                </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Phone </label>
	                                <div class="col-md-7">
	                                    <input name="phone" type="text" class="form-control" placeholder="eg. 09-338XXXX">
	                                </div>
	                        </div>

	                         <div class="form-group">
	                            <label class="col-md-4 control-label"> Address </label>
	                                <div class="col-md-7">
	                                    <input name="address" type="text" class="form-control" placeholder="eg. Sangyaung, Yangon">
	                                </div>
	                        </div>

	                         <div class="form-group">
	                            <label class="col-md-4 control-label">  </label>
	                                <div class="col-md-7">
	                                    <button type="button" class="btn btn-primary fa fa-save" onclick="addUser()"> Save </button>
	                                    <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                                </div>
	                        </div>
	                                            
	                                            
	                           
	                       </form>
		</div>
	</div>
</div>

<!-- Modal -->
 <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
            <div class="modal-content"> 
                <div class="modal-header"> 
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h4 class="modal-title"> Staff Registration </h4> 
                </div> 
            <div class="modal-body"> 
                <div class="row"> 
                    <div class="col-md-12"> 
                       <form class="form-horizontal" id="staffRegistraion" role="form">                                    
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Name </label>
	                                <div class="col-md-7">
	                                    <input name="name" type="text" class="form-control" placeholder="eg. Mr.Smith">
	                                </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Username </label>
	                                <div class="col-md-7">
	                                    <input name="username" type="text" class="form-control" placeholder="smith23">
	                                </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Password </label>
	                                <div class="col-md-7">
	                                    <input name="password" type="password" class="form-control" placeholder="eg. smith#@12">
	                                </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-md-4 control-label"> Phone </label>
	                                <div class="col-md-7">
	                                    <input name="phone" type="text" class="form-control" placeholder="eg. 09-338XXXX">
	                                </div>
	                        </div>

	                         <div class="form-group">
	                            <label class="col-md-4 control-label"> Address </label>
	                                <div class="col-md-7">
	                                    <input name="address" type="text" class="form-control" placeholder="eg. Sangyaung, Yangon">
	                                </div>
	                        </div>

	                         <div class="form-group">
	                            <label class="col-md-4 control-label">  </label>
	                                <div class="col-md-7">
	                                    <button type="button" class="btn btn-primary fa fa-save" onclick="addUser()"> Save </button>
	                                    <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                                </div>
	                        </div>
	                                            
	                                            
	                           
	                       </form>
		             </div>      
		        </div> 
		    </div>
</div><!-- /.modal -->

