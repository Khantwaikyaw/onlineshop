    <div class="modal-header"> 
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h4 class="modal-title"> New Brand </h4> 
                </div> 
 <div class="modal-body">  
  <div class="row"> 
        <div class="col-md-12"> 
            <form class="form-horizontal" name="brand" role="form" enctype="multipart/form-data">                                    
	             <div class="form-group">
	                    <label class="col-md-4 control-label"> Brand Name </label>
	                        <div class="col-md-5">
	                            <input name="brandName" type="text" class="form-control" placeholder="eg. Something">
	                        </div>
	                        <div class="col-md-3">
	                            <label class="btn-bs-file btn  btn-primary">
					                Brand Logo
					                <input type="file" id="logo" name="brandLogo" required>
					            </label>
	                        </div>
	            </div>
	            <div class="form-group">
	            	<div class="img-box">
	            		<p id="img-preview">Preview</p>
	            		<img id="sample-img" />
	            	</div>
	            </div>
	          
	        

	                         <div class="form-group">
	                            <label class="col-md-4 control-label">  </label>
	                                <div class="col-md-7">
	                                    <button type="submit" class="btn btn-primary fa fa-save"> Add </button>
	                                    <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                                </div>
	                        </div>
	                                            
	                                            
	                           
	                       </form>
		             </div>      
		        </div>  
</div>
 <!-- Custom -->
         <script src="<?php echo base_url('assets/custom/admin/js/method.js'); ?>"></script>
