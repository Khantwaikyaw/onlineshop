<div class="row">
		
	<div class="panel panel-border panel-primary">
		
		<div class="panel-heading">
			<h3 class="panel-title"> Brand List </h3>
		</div>
		<div class="panel-body">

			<div class="form-group">
				<button onclick="Modal('<?php echo base_url('brand/brand/new'); ?>')" class="btn btn-primary waves-effect waves-light fa fa-plus" data-toggle="modal" data-target="#con-close-modal" type="button"> Create </button>
			</div>

			<table id="datatable" class="table table-striped table-bordered table-brand">
                                <thead>
                                <tr>
                                    <th> 	Brand Name 	</th>
                                    <th>	Action 		</th>
                                 </tr>
                                </thead>


                                <tbody>
                               <?php foreach ($brandList->result() as $row): ?>

                               	<tr>
                               		<td>
                               		 	<?php echo $row->brandName; ?>
                               		 	<img class="img-brandList" src="<?php echo base_url('assets/uploads/brand/'.$row->brandLogo); ?>">	
                               		 </td>
                               		<td> 
                               			<button class="btn btn-info btn-sm btn-brandList fa fa-edit" > Edit </button>
                               			<button class="btn btn-danger btn-sm btn-brandList fa fa-trash" > Delete </button>
                               		</td>
                               	</tr>
                               
                               <?php endforeach ?>
                                
                                </tbody>
                            </table>
		</div>
	</div>
</div>



