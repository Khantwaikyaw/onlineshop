<div class="row">
		
	<div class="panel panel-border panel-primary">
		
		<div class="panel-heading">
			<h3 class="panel-title"> Staff List </h3>
		</div>
		<div class="panel-body">

			<div class="form-group">
				<button onclick="Modal('<?php echo base_url('staff/staff/new'); ?>')" class="btn btn-primary waves-effect waves-light fa fa-plus" data-toggle="modal" data-target="#con-close-modal" type="button"> Create </button>
			</div>

			<table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Staff ID</th>
                                    <th>Staff Name</th>
                                    <th>UserName</th>
                                    <th>Passowrd</th>
                                    <th>Phone</th>
                                </tr>
                                </thead>


                                <tbody>
                               <?php foreach ($staffdata->result() as $row): ?>

                               	<tr>
                               		<td> <?php echo $row->staffId; ?></td>
                               		<td> <?php echo $row->name; ?></td>
                               		<td> <?php echo $row->username; ?></td>
                               		<td> <?php echo $row->password; ?></td>
                               		<td> <?php echo $row->phone; ?></td>
                               	</tr>
                               
                               <?php endforeach ?>
                                
                                </tbody>
                            </table>
		</div>
	</div>
</div>





