<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	private $path = 'Staff/';

	public function __construct()
	{
		parent::__construct();
			$this->load->model('Model_Staff');
	}

	public function index()
	{
		$staff = new Model_Staff();

		$data['staffdata'] = $staff->getData();
		$data['pages'] = $this->path.'staff_List';

		$this->load->view( 'admin/index', $data );

	}

	public function staff($para1='')
	{
		$this->load->model('Model_Staff');

		$staff = new Model_Staff();

		if ( $para1 == 'new') {

	 		$this->load->view('admin/pages/Staff/staff_New');

	 	}
		elseif ( $para1 == 'add') 
		{
			

			$staff->setData(	$this->input->post('name'),
								$this->input->post('username'),
								$this->input->post('password'),
								$this->input->post('phone'),
								$this->input->post('address')
							);

			return ture;

		}
		else
		{
			$staff = new Model_Staff();

		$data['staffdata'] = $staff->getData();
		$data['pages'] = $this->path.'staff_List';

		$this->load->view( 'admin/index', $data );
		}

	}
	
}
