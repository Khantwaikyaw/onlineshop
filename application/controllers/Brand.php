<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {

	private $path = 'Brand/';

	public function __construct()
	{
		parent::__construct();
			$this->load->model('Model_Brand');
	}

	public function index()
	{
		$brand = new Model_Brand();

		$data['brandList'] = $brand->getData();
		$data['pages'] = $this->path.'brand_List';

		$this->load->view( 'admin/index', $data );

	}

	public function brand( $para1 = '')
	{
		$brand = new Model_Brand();

		if ( $para1 == 'new' )
		{
			$this->load->view('admin/pages/Brand/brand_New');
		}
		elseif ( $para1 == 'add')
		{
			$config['upload_path'] = './assets/uploads/brand/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';

			$this->load->library('upload',$config);// Call upload library

			if ( ! $this->upload->do_upload('brandLogo'))
			{
				echo $this->upload->display_errors();
			}
			else
			{

				$imagedata=$this->upload->data();
				$imagedata = $imagedata['file_name'];

				$brand->SetData( 	$this->input->post('brandName'),
									$imagedata
								);

				
			}
		}
		else
		{
			$brand = new Model_Brand();

			$data['brandList'] = $brand->getData();
			$data['pages'] = $this->path.'brand_List';

			$this->load->view( 'admin/index', $data );
		}
	}
	
}
