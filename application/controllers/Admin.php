<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pages'] = 'test';
		$this->load->view('admin/index.php',$data);
	}

	public function staff($para1='')
	{
		$this->load->model('Staff');

			$staff = new Staff();
		if ( $para1 == 'list')
		{
			
			$data['pages'] = 'staff_List';
			$data['staffdata'] =  $staff->getData();
			
			$this->load->view('admin/index.php',$data);

		}elseif ( $para1 == 'new') {

	 		$this->load->view('admin/pages/staff_New');

	 	}else if( $para1 == 'terms'){

			$data['pages'] = 'Terms';
			$this->load->view('admin/index.php',$data);

		}
		elseif ( $para1 == 'add') 
		{
			

			$staff->setData(	$this->input->post('name'),
								$this->input->post('username'),
								$this->input->post('password'),
								$this->input->post('phone'),
								$this->input->post('address')
							);

			return ture;

		}

	}



	public function items( $para1 = '')
	{
		if ( $para1 == 'list')
		{
			echo "something";
			
		}
		else
		{
			$data['pages'] = 'items';
			$this->load->view('admin/index',$data);
		}
	}

	public function category($para1 = '')
	{
	 	if( $para1 == 'list' ){
	 		$data['pages'] ='category_List';
	 		$this->load->view('admin/index',$data);
	 	}
	 	elseif ( $para1 == 'new') 
	 	{
	 		$this->load->view('admin/pages/category_New');
	 	}
	 	elseif( $para1 == 'add')
	 	{
	 		// echo $this->input->post('categoryName');
	 		$this->load->model('Category');

			$category = new Category();

			$category->setData(	$this->input->post('categoryName'));

			return ture;
	 	}
	}
}
