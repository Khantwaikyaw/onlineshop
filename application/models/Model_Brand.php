<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Brand {

	protected $CI;
	
	protected $table;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->table = 'brand';
	}

	public function setData( $brandName, $brandLogo)
	{

		$data = array(	'brandName' => $brandName,
						'brandLogo' => $brandLogo );

		$this->CI->CRUD->create( $this->table, $data);
	}

	public function getData()
	{
		return $this->CI->CRUD->read( $this->table );
	}
}
