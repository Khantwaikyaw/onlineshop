<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Staff {

	protected $CI;
	protected $table;
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->table = "staff";
	}

	public function setData($name,$username,$password,$phone,$address)
	{


		$data = array(	'name'			=> $name ,
						'username'		=> $username,
						'password'		=> $password,
						'hashPassword'	=> sha1($password),
						'phone'			=> $phone,
						'address'		=> $address
						 );

		$this->CI->CRUD->create($this->table,$data);

	}


	public function getData(){
		return $this->CI ->CRUD->read($this->table);
	}
}
