<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CRUD extends CI_Model {

	function create($table,$data)
	{
		$this->db->insert($table,$data);
	}

	function read($table){
	  return $this->db->get($table);
	}
	
}
