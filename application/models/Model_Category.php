<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category {

	protected $CI;
	
	protected $table;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->table = 'category';
	}

	public function setData($cateogryName)
	{

		$data = array(	'categoryName' => $cateogryName );

		$this->CI->CRUD->create( $this->table, $data);

	}
}
